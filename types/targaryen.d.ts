
declare module "targaryen" {
  export interface Ruleset {
    rules: object;
  }
  export type Dataset = object;
  export type Auth = object | null;
  export type Timestamp = number;
  export interface ReadResult<S extends Dataset> {
    path: string;
    auth: Auth;
    type: "read" | "write" | "patch";
    allowed: boolean;
    info: any;
    database: Database<S>;
  }
  export interface WriteResult<S extends Dataset> extends ReadResult<S> {
    newDatabase?: Database<S>;
    newValue?: any;
  }
  export interface Database<S extends Dataset> {
    with(alt: {
      rules?: Ruleset;
      data?: S;
      auth?: Auth;
      now?: Timestamp;
      debug?: boolean;
    }): Database<S>;
    as(auth?: Auth): Database<S>;
    read(path: string, options?: {
      now?: Timestamp;
      query?: object;
    }): ReadResult<S>;
    write(path: string, value: any, options?: {
      now?: Timestamp;
      priority?: any;
    }): WriteResult<S>;
    update(path: string, patch: any, options?: {
      now?: Timestamp;
    }): WriteResult<S>;
  }
  export function database<S extends Dataset>(rules: Ruleset, data: S, now?: Timestamp): Database<S>;
}
