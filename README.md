# realtime-db-adaptor

`RealtimeDatabase` is a JSON data store that allows for active subscriptions to data events.

It is primarily modeled after (and a wrapper around) Firebase's [Realtime Database](https://firebase.google.com/products/realtime-database/).

## Installation

Simply install as a dependency: `npm i -E realtime-db-adaptor`.

## Usage

This adaptor is transpiled as a `commonjs` module. It will be natively consumed if used in a `node` environment, and can be easily bundled for use on the web.

Here's the simplest example of constructing a `RealtimeDatabase` instance from an active `firebase` database. Keep in mind that the type argument is applied on top of the database. This wrapper will not provide runtime checks to make sure that the schema you pass in actually matches the structure of your database.

```typescript
import firebase from "firebase";
import { wrapFirebaseDatabase } from "realtime-db-adaptor";

interface MySchema {
  table1: { [key: string]: number; };
  table2: { [key: string]: string; };
}

const db = wrapFirebaseDatabase<MySchema>(firebase.database());
```

## Documentation

The whole purpose of this module is to provide a strong TypeScript experience when using the Realtime Database. The `RealtimeDatabase` type and all of the relevant ancillary types can be found at [the generated documentation site](https://mmolisani.gitlab.io/realtime-db-adaptor/index.html).
