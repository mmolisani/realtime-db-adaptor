import { runTestSuite } from "./database.spec";
import { buildMockDatabase } from "./mock";

runTestSuite("mock", async (...args) => buildMockDatabase(...args));
