import * as admin from "firebase-admin";
import { Observable, Subject } from "rxjs";
import { Entry, RealtimeDatabase, RealtimeDatabaseChildReference } from "./database";

export const asPath = (lookup: string[]) => `/${lookup.join("/")}`;

export const serialize = <T>(value: T): T => {
  return value && JSON.parse(JSON.stringify(value));
};

interface FirebaseDataSnapshot {
  key: string | null;
  val(): any;
  // needed for type coercion
  child(path: string): FirebaseDataSnapshot;
  exists(): boolean;
  exportVal(): any;
  forEach(action: (a: FirebaseDataSnapshot) => boolean | void): boolean;
  getPriority(): string | number | null;
  hasChild(path: string): boolean;
  hasChildren(): boolean;
  numChildren(): number;
  ref: any;
  toJSON(): object | null;
}
interface FirebaseReference {
  key: string | null;

  on(
    eventType: firebase.database.EventType,
    callback: (a: FirebaseDataSnapshot | null, b?: any) => any,
    cancelCallbackOrContext?: object | null,
    context?: object | null
  ): (a: FirebaseDataSnapshot | null, b?: any) => any;

  once(
    eventType: firebase.database.EventType,
    callback?: (a: FirebaseDataSnapshot, b?: string | null) => any,
    failureCallbackOrContext?: ((a: Error) => void) | object | null,
    context?: object | null
  ): Promise<FirebaseDataSnapshot>;

  push(
    value?: any,
    onComplete?: (a: Error | null) => any
  ): FirebaseThenableReference;

  set(
    value: any,
    onComplete?: (a: Error | null) => any
  ): Promise<any>;

  update(
    values: object,
    onComplete?: (a: Error | null) => any
  ): Promise<any>;

  transaction(
    transactionUpdate: (a: any) => any,
    onComplete?: (a: Error | null, b: boolean, c: FirebaseDataSnapshot | null) => any,
    applyLocally?: boolean
  ): Promise<any>;

  remove(
    onComplete?: (a: Error | null) => any
  ): Promise<any>;
}
interface FirebaseThenableReference extends FirebaseReference, Promise<FirebaseReference> {}
interface FirebaseDatabase {
  ref(path?: string): FirebaseReference;
}

const streamEvent = <T>(ref: FirebaseReference, event: firebase.database.EventType, map: (snapshot: FirebaseDataSnapshot) => T) => {
  const subject = new Subject<T>();
  ref.on(event, (snapshot) => {
    if (snapshot !== null) {
      subject.next(map(snapshot));
    }
  }, (err?: Error) => {
    subject.error(err);
  });
  return subject.asObservable();
};

const extractEntryFromSnapshot = (snapshot: FirebaseDataSnapshot): Entry<any, any> => {
  return { key: snapshot.key, value: snapshot.val() };
};

function getDatabaseAuth(db: firebase.database.Database | admin.database.Database): firebase.UserInfo | null {
  const auth = db.app.auth();
  if ("currentUser" in auth) {
    return auth.currentUser;
  }
  return (auth.app.options.databaseAuthVariableOverride as firebase.UserInfo | undefined) || null;
}

class FirebaseRealtimeDatabase<SCHEMA extends object> implements RealtimeDatabase<SCHEMA> {
  private _db: FirebaseDatabase;
  private _authUserInfo: firebase.UserInfo | null;
  constructor(db: firebase.database.Database | admin.database.Database) {
    this._db = db;
    this._authUserInfo = getDatabaseAuth(db);
  }
  get auth(): firebase.UserInfo | null {
    return this._authUserInfo;
  }
  async get(lookup: string[]): Promise<any | null> {
    const snapshot = await this._db.ref(asPath(lookup)).once("value");
    return snapshot.val();
  }
  async push(lookup: string[], value: any): Promise<string | null> {
    return this._db.ref(asPath(lookup)).push(serialize(value)).key;
  }
  set(lookup: string[], value: any): Promise<void> {
    return this._db.ref(asPath(lookup)).set(serialize(value));
  }
  update(lookup: string[], value: any): Promise<void> {
    return this._db.ref(asPath(lookup)).update(serialize(value));
  }
  async tx(lookup: string[], update: (currentValue: any) => any | undefined): Promise<boolean> {
    const result = await this._db.ref(asPath(lookup)).transaction((currentValue) => {
      const newValue = update(currentValue);
      if (newValue !== void 0) {
        return serialize(newValue);
      }
    });
    return result.committed;
  }
  remove(lookup: string[]): Promise<void> {
    return this._db.ref(asPath(lookup)).remove();
  }
  observe(lookup: string[]): Observable<any | null> {
    const ref = this._db.ref(asPath(lookup));
    return streamEvent(ref, "value", (snapshot) => snapshot.val());
  }
  children(lookup: string[]): RealtimeDatabaseChildReference<any, string, any> {
    const ref = this._db.ref(asPath(lookup));
    return {
      lookup,
      added() {
        return streamEvent(ref, "child_added", extractEntryFromSnapshot);
      },
      removed() {
        return streamEvent(ref, "child_removed", extractEntryFromSnapshot);
      },
      changed() {
        return streamEvent(ref, "child_changed", extractEntryFromSnapshot);
      },
    };
  }
}

export function wrapFirebaseDatabase<SCHEMA extends object>(db: firebase.database.Database | admin.database.Database): RealtimeDatabase<SCHEMA> {
  return new FirebaseRealtimeDatabase<SCHEMA>(db);
}
