import * as _firebase from "@firebase/testing";
import { Changeset, RealtimeDatabase } from "./database";
import { Ruleset, runTestSuite } from "./database.spec";
import { wrapFirebaseDatabase } from "./firebase";

const DEFAULT_RULES = {
  ".read": true,
  ".write": true,
};

async function buildFirebaseDatabase<SCHEMA extends object>(data: Changeset<SCHEMA> = {}, rules: Ruleset<SCHEMA> = DEFAULT_RULES as Ruleset<SCHEMA>, auth?: object): Promise<RealtimeDatabase<SCHEMA>> {
  const dbName = `realtime-db-adaptor-test-${Date.now()}`;
  const app = _firebase.initializeTestApp({
    databaseName: dbName,
    projectId: dbName,
    auth,
  });
  const db = app.database();
  db.app.auth = () => ({ currentUser: auth || null } as any);
  if (data !== void 0) {
    await _firebase.loadDatabaseRules({
      databaseName: dbName,
      rules: JSON.stringify({ rules: DEFAULT_RULES }),
    });
    await db.ref().set(data);
  }
  await _firebase.loadDatabaseRules({
    databaseName: dbName,
    rules: JSON.stringify({ rules }),
  });
  return wrapFirebaseDatabase<SCHEMA>(db);
}

before(async function() {
  this.timeout(4000);
  const initDb = await buildFirebaseDatabase<{ __startup_canary__: never }>({});
  await initDb.remove(["__startup_canary__"]);
});

after(async function() {
  this.timeout(4000);
  await Promise.all(_firebase.apps().map((app) => app.delete()));
});

runTestSuite("firebase", buildFirebaseDatabase);
