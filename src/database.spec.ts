import { expect } from "chai";
import { Subject } from "rxjs";
import { toArray } from "rxjs/operators";
import { Changeset, Entry, RealtimeDatabase } from "./database";

export interface Rules {
  ".read"?: boolean | string;
  ".write"?: boolean | string;
  ".validate"?: boolean | string;
  ".indexOn"?: string[];
}

export type Ruleset<SCHEMA> = Rules & { [K in Exclude<keyof SCHEMA, keyof Rules>]?: Ruleset<SCHEMA[K]> };

interface SimpleSchema {
  a: {
    b: {
      c: {
        d1?: number;
        d2?: number;
        d3?: number;
      };
    };
  };
}

interface IndexSchema {
  [a: string]: {
    [b: string]: {
      [c: string]: {
        [d: string]: number;
      };
    };
  };
}

export type DatabaseBuilder = <SCHEMA extends object>(data?: Changeset<SCHEMA>, ruleset?: object, auth?: { uid: string }) => Promise<RealtimeDatabase<SCHEMA>>;

export function runTestSuite(name: string, dbBuilder: DatabaseBuilder) {

  describe(`${name} database`, () => {

    describe("auth", () => {

      it("none", async () => {
        const db = await dbBuilder(void 0, void 0, void 0);
        expect(db.auth).to.equal(null);
      });

      it("valid user", async () => {
        const db = await dbBuilder(void 0, void 0, { uid: "user-a" });
        expect(db.auth).to.have.property("uid", "user-a");
      });

    });

    describe("simple schema", () => {

      describe("get from", () => {

        it("/a", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          const value = await db.get(["a"]);
          expect(value).to.deep.equal({
            b: {
              c: {
                d1: 0,
                d2: 0,
                d3: 0,
              },
            },
          });
        });

        it("/a/b", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          const value = await db.get(["a", "b"]);
          expect(value).to.deep.equal({
            c: {
              d1: 0,
              d2: 0,
              d3: 0,
            },
          });
        });

        it("/a/b/c", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          const value = await db.get(["a", "b", "c"]);
          expect(value).to.deep.equal({
            d1: 0,
            d2: 0,
            d3: 0,
          });
        });

        it("/a/b/c/d1", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          const value = await db.get(["a", "b", "c", "d1"]);
          expect(value).to.deep.equal(0);
        });

      });

      describe("set at", () => {

        it("/a", async () => {
          const db = await dbBuilder<SimpleSchema>();
          const expected = {
            b: {
              c: {
                d1: 1,
                d2: 2,
                d3: 3,
              },
            },
          };
          await db.set(["a"], expected);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal(expected);
        });

        it("/a/b", async () => {
          const db = await dbBuilder<SimpleSchema>();
          const expected = {
            c: {
              d1: 1,
              d2: 2,
              d3: 3,
            },
          };
          await db.set(["a", "b"], expected);
          const value = await db.get(["a", "b"]);
          expect(value).to.deep.equal(expected);
        });

        it("/a/b/c", async () => {
          const db = await dbBuilder<SimpleSchema>();
          const expected = {
            d1: 1,
            d2: 2,
            d3: 3,
          };
          await db.set(["a", "b", "c"], expected);
          const value = await db.get(["a", "b", "c"]);
          expect(value).to.deep.equal(expected);
        });

        it("/a/b/c/d1", async () => {
          const db = await dbBuilder<SimpleSchema>();
          const expected = 1;
          await db.set(["a", "b", "c", "d1"], expected);
          const value = await db.get(["a", "b", "c", "d1"]);
          expect(value).to.deep.equal(expected);
        });

      });

      describe("update at", () => {

        it("/a", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          const partialValue = {
            b: {
              c: {
                d1: 1,
              },
            },
          };
          await db.update(["a"], partialValue);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal({
            b: {
              c: {
                d1: 1,
              },
            },
          });
        });

        it("/a/b", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          const partialValue = {
            c: {
              d1: 1,
            },
          };
          await db.update(["a", "b"], partialValue);
          const value = await db.get(["a", "b"]);
          expect(value).to.deep.equal({
            c: {
              d1: 1,
            },
          });
        });

        it("/a/b/c", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          const partialValue = {
            d1: 1,
          };
          await db.update(["a", "b", "c"], partialValue);
          const value = await db.get(["a", "b", "c"]);
          expect(value).to.deep.equal({
            d1: 1,
            d2: 0,
            d3: 0,
          });
        });

      });

      describe("tx", () => {

        describe.skip("modifies existing at", () => {

          it("/a", async () => {
            const db = await dbBuilder<SimpleSchema>({
              a: {
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              },
            });
            await db.tx(["a"], (currentValue) => {
              expect(currentValue).to.deep.equal({
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              });
              if (currentValue === null) {
                return;
              }
              currentValue.b.c.d2 = (currentValue.b.c.d2 || 0) + 1;
              return currentValue;
            });
            const value = await db.get(["a"]);
            expect(value).to.deep.equal({
              b: {
                c: {
                  d1: 0,
                  d2: 1,
                  d3: 0,
                },
              },
            });
          });

          it("/a/b", async () => {
            const db = await dbBuilder<SimpleSchema>({
              a: {
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              },
            });
            await db.tx(["a", "b"], (currentValue) => {
              expect(currentValue).to.deep.equal({
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              });
              if (currentValue === null) {
                return;
              }
              currentValue.c.d2 = (currentValue.c.d2 || 0) + 1;
              return currentValue;
            });
            const value = await db.get(["a", "b"]);
            expect(value).to.deep.equal({
              c: {
                d1: 0,
                d2: 1,
                d3: 0,
              },
            });
          });

          it("/a/b/c", async () => {
            const db = await dbBuilder<SimpleSchema>({
              a: {
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              },
            });
            await db.tx(["a", "b", "c"], (currentValue) => {
              expect(currentValue).to.deep.equal({
                d1: 0,
                d2: 0,
                d3: 0,
              });
              if (currentValue === null) {
                return;
              }
              currentValue.d2 = (currentValue.d2 || 0) + 1;
              return currentValue;
            });
            const value = await db.get(["a", "b", "c"]);
            expect(value).to.deep.equal({
              d1: 0,
              d2: 1,
              d3: 0,
            });
          });

          it("/a/b/c/d2", async () => {
            const db = await dbBuilder<SimpleSchema>({
              a: {
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              },
            });
            await db.tx(["a", "b", "c", "d2"], (currentValue) => {
              expect(currentValue).to.deep.equal(0);
              if (currentValue === null) {
                return;
              }
              return (currentValue || 0) + 1;
            });
            const value = await db.get(["a", "b", "c", "d2"]);
            expect(value).to.deep.equal(1);
          });

        });

        describe("does nothing at", () => {

          it("/a", async () => {
            const db = await dbBuilder<SimpleSchema>({
              a: {
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              },
            });
            await db.tx(["a"], () => void 0);
            const value = await db.get(["a"]);
            expect(value).to.deep.equal({
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            });
          });

          it("/a/b", async () => {
            const db = await dbBuilder<SimpleSchema>({
              a: {
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              },
            });
            await db.tx(["a", "b"], () => void 0);
            const value = await db.get(["a", "b"]);
            expect(value).to.deep.equal({
              c: {
                d1: 0,
                d2: 0,
                d3: 0,
              },
            });
          });

          it("/a/b/c", async () => {
            const db = await dbBuilder<SimpleSchema>({
              a: {
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              },
            });
            await db.tx(["a", "b", "c"], () => void 0);
            const value = await db.get(["a", "b", "c"]);
            expect(value).to.deep.equal({
              d1: 0,
              d2: 0,
              d3: 0,
            });
          });

          it("/a/b/c/d2", async () => {
            const db = await dbBuilder<SimpleSchema>({
              a: {
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              },
            });
            await db.tx(["a", "b", "c", "d2"], () => void 0);
            const value = await db.get(["a", "b", "c", "d2"]);
            expect(value).to.deep.equal(0);
          });

        });

      });

      describe("remove from", () => {

        it("/a/b/c/d1", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          await db.remove(["a", "b", "c", "d1"]);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal({
            b: {
              c: {
                d2: 0,
                d3: 0,
              },
            },
          });
        });

        it("/a/b/c", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          await db.remove(["a", "b", "c"]);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal(null);
        });

        it("/a/b", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          await db.remove(["a", "b"]);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal(null);
        });

        it("/a", async () => {
          const db = await dbBuilder<SimpleSchema>({
            a: {
              b: {
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              },
            },
          });
          await db.remove(["a"]);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal(null);
        });

      });

    });

    describe("index schema", () => {

      describe("get from", () => {

        it("/a (empty)", async () => {
          const db = await dbBuilder<IndexSchema>();
          const value = await db.get(["a"]);
          expect(value).to.deep.equal(null);
        });

        it("/a/b (empty)", async () => {
          const db = await dbBuilder<IndexSchema>();
          const value = await db.get(["a", "b"]);
          expect(value).to.deep.equal(null);
        });

        it("/a/b/c (empty)", async () => {
          const db = await dbBuilder<IndexSchema>();
          const value = await db.get(["a", "b", "c"]);
          expect(value).to.deep.equal(null);
        });

        it("/a/b/c/d1 (empty)", async () => {
          const db = await dbBuilder<IndexSchema>();
          const value = await db.get(["a", "b", "c", "d1"]);
          expect(value).to.deep.equal(null);
        });

      });

      describe("push to", () => {

        it("/a", async () => {
          const db = await dbBuilder<IndexSchema>();
          const expected = {
            c: {
              d1: 1,
              d2: 2,
              d3: 3,
            },
          };
          const key = await db.push(["a"], expected);
          expect(key).to.be.a("string");
          const value = await db.get(["a", key as any]);
          expect(value).to.deep.equal(expected);
        });

        it("/a/b", async () => {
          const db = await dbBuilder<IndexSchema>();
          const expected = {
            d1: 1,
            d2: 2,
            d3: 3,
          };
          const key = await db.push(["a", "b"], expected);
          expect(key).to.be.a("string");
          const value = await db.get(["a", "b", key as any]);
          expect(value).to.deep.equal(expected);
        });

        it("/a/b/c", async () => {
          const db = await dbBuilder<IndexSchema>();
          const expected = 1;
          const key = await db.push(["a", "b", "c"], expected);
          expect(key).to.be.a("string");
          const value = await db.get(["a", "b", "c", key as any]);
          expect(value).to.deep.equal(expected);
        });

      });

      describe("set at", () => {

        it("/a", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b1: {
                c1: {
                  d1: 111,
                  d2: 112,
                },
                c2: {
                  d1: 121,
                  d2: 122,
                },
              },
              b2: {
                c1: {
                  d1: 211,
                  d2: 212,
                },
                c2: {
                  d1: 221,
                  d2: 222,
                },
              },
            },
          });
          const expected = {
            b1: {
              c1: {
                d1: 1110,
                d2: 1120,
              },
              c2: {
                d1: 1210,
                d2: 1220,
              },
            },
          };
          await db.set(["a"], expected);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal(expected);
        });

        it("/a/b", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b: {
                c1: {
                  d1: 11,
                  d2: 12,
                },
                c2: {
                  d1: 21,
                  d2: 22,
                },
              },
            },
          });
          const expected = {
            c1: {
              d1: 110,
              d2: 120,
            },
          };
          await db.set(["a", "b"], expected);
          const value = await db.get(["a", "b"]);
          expect(value).to.deep.equal(expected);
        });

        it("/a/b/c", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b: {
                c: {
                  d1: 1,
                  d2: 2,
                },
              },
            },
          });
          const expected = {
            d1: 10,
          };
          await db.set(["a", "b", "c"], expected);
          const value = await db.get(["a", "b", "c"]);
          expect(value).to.deep.equal(expected);
        });

        it("/a/b/c/d1", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b: {
                c: {
                  d: 1,
                },
              },
            },
          });
          const expected = 10;
          await db.set(["a", "b", "c", "d1"], expected);
          const value = await db.get(["a", "b", "c", "d1"]);
          expect(value).to.deep.equal(expected);
        });

      });

      describe("update at", () => {

        it("/a", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b1: {
                c1: {
                  d1: 111,
                  d2: 112,
                },
                c2: {
                  d1: 121,
                  d2: 122,
                },
              },
              b2: {
                c1: {
                  d1: 211,
                  d2: 212,
                },
                c2: {
                  d1: 221,
                  d2: 222,
                },
              },
            },
          });
          const expected = {
            b1: {
              c1: {
                d1: 1110,
                d2: 1120,
              },
              c2: {
                d1: 1210,
                d2: 1220,
              },
            },
          };
          await db.update(["a"], expected);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal({
            b1: {
              c1: {
                d1: 1110,
                d2: 1120,
              },
              c2: {
                d1: 1210,
                d2: 1220,
              },
            },
            b2: {
              c1: {
                d1: 211,
                d2: 212,
              },
              c2: {
                d1: 221,
                d2: 222,
              },
            },
          });
        });

        it("/a/b", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b: {
                c1: {
                  d1: 11,
                  d2: 12,
                },
                c2: {
                  d1: 21,
                  d2: 22,
                },
              },
            },
          });
          const expected = {
            c1: {
              d1: 110,
              d2: 120,
            },
          };
          await db.update(["a", "b"], expected);
          const value = await db.get(["a", "b"]);
          expect(value).to.deep.equal({
            c1: {
              d1: 110,
              d2: 120,
            },
            c2: {
              d1: 21,
              d2: 22,
            },
          });
        });

        it("/a/b/c", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b: {
                c: {
                  d1: 1,
                  d2: 2,
                },
              },
            },
          });
          const expected = {
            d1: 10,
          };
          await db.update(["a", "b", "c"], expected);
          const value = await db.get(["a", "b", "c"]);
          expect(value).to.deep.equal({
            d1: 10,
            d2: 2,
          });
        });

      });

      describe("remove from", () => {

        it("/a/b/c/d1", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b1: {
                c1: {
                  d1: 111,
                  d2: 112,
                },
                c2: {
                  d1: 121,
                  d2: 122,
                },
              },
              b2: {
                c1: {
                  d1: 211,
                  d2: 212,
                },
                c2: {
                  d1: 221,
                  d2: 222,
                },
              },
            },
          });
          await db.remove(["a", "b1", "c1", "d1"]);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal({
            b1: {
              c1: {
                d2: 112,
              },
              c2: {
                d1: 121,
                d2: 122,
              },
            },
            b2: {
              c1: {
                d1: 211,
                d2: 212,
              },
              c2: {
                d1: 221,
                d2: 222,
              },
            },
          });
        });

        it("/a/b/c", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b1: {
                c1: {
                  d1: 111,
                  d2: 112,
                },
                c2: {
                  d1: 121,
                  d2: 122,
                },
              },
              b2: {
                c1: {
                  d1: 211,
                  d2: 212,
                },
                c2: {
                  d1: 221,
                  d2: 222,
                },
              },
            },
          });
          await db.remove(["a", "b1", "c1"]);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal({
            b1: {
              c2: {
                d1: 121,
                d2: 122,
              },
            },
            b2: {
              c1: {
                d1: 211,
                d2: 212,
              },
              c2: {
                d1: 221,
                d2: 222,
              },
            },
          });
        });

        it("/a/b", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b1: {
                c1: {
                  d1: 111,
                  d2: 112,
                },
                c2: {
                  d1: 121,
                  d2: 122,
                },
              },
              b2: {
                c1: {
                  d1: 211,
                  d2: 212,
                },
                c2: {
                  d1: 221,
                  d2: 222,
                },
              },
            },
          });
          await db.remove(["a", "b1"]);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal({
            b2: {
              c1: {
                d1: 211,
                d2: 212,
              },
              c2: {
                d1: 221,
                d2: 222,
              },
            },
          });
        });

        it("/a", async () => {
          const db = await dbBuilder<IndexSchema>({
            a: {
              b1: {
                c1: {
                  d1: 111,
                  d2: 112,
                },
                c2: {
                  d1: 121,
                  d2: 122,
                },
              },
              b2: {
                c1: {
                  d1: 211,
                  d2: 212,
                },
                c2: {
                  d1: 221,
                  d2: 222,
                },
              },
            },
          });
          await db.remove(["a"]);
          const value = await db.get(["a"]);
          expect(value).to.deep.equal(null);
        });

      });

      describe("observe", () => {

        describe("(of set, update, remove, push)", () => {

          describe("on /a while observing", () => {

            async function performActions(lookup: string[]) {
              const db = await dbBuilder<IndexSchema>({
                a: {
                  b: {
                    c: {
                      d1: 0,
                      d2: 0,
                      d3: 0,
                    },
                  },
                },
              });
              const valuesSubject = new Subject<any | null>();
              db.observe(lookup as any).subscribe(valuesSubject);
              const valuesPromise = valuesSubject.pipe(toArray()).toPromise();
              await db.remove(["__force_db_tick__"]);
              const expectSet = {
                b: {
                  c: {
                    d1: 1,
                    d2: 1,
                    d3: 1,
                  },
                },
              };
              await db.set(["a"], expectSet);
              const expectUpdate = {
                b: {
                  c: {
                    d2: 2,
                  },
                },
              };
              await db.update(["a"], expectUpdate);
              await db.remove(["a"]);
              const expectPush = {
                c: {
                  d1: 3,
                  d2: 3,
                  d3: 3,
                },
              };
              const pushKey = await db.push(["a"], expectPush) as string;
              expect(pushKey).to.be.a("string");
              valuesSubject.complete();
              return valuesPromise;
            }

            it("/a", async () => {
              const values = await performActions(["a"]);
              expect(values).to.have.length(5);
              expect(values[0]).to.deep.equal({
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              });
              expect(values[1]).to.deep.equal({
                b: {
                  c: {
                    d1: 1,
                    d2: 1,
                    d3: 1,
                  },
                },
              });
              expect(values[2]).to.deep.equal({
                b: {
                  c: {
                    d2: 2,
                  },
                },
              });
              expect(values[3]).to.deep.equal(null);
              const [key] = Object.keys(values[4]);
              expect(key).to.be.a("string");
              expect(values[4]).to.deep.equal({
                [key]: {
                  c: {
                    d1: 3,
                    d2: 3,
                    d3: 3,
                  },
                },
              });
            });

            it("/a/b", async () => {
              const values = await performActions(["a", "b"]);
              expect(values).to.have.length(4);
              expect(values[0]).to.deep.equal({
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              });
              expect(values[1]).to.deep.equal({
                c: {
                  d1: 1,
                  d2: 1,
                  d3: 1,
                },
              });
              expect(values[2]).to.deep.equal({
                c: {
                  d2: 2,
                },
              });
              expect(values[3]).to.deep.equal(null);
            });

            it("/a/b/c", async () => {
              const values = await performActions(["a", "b", "c"]);
              expect(values).to.have.length(4);
              expect(values[0]).to.deep.equal({
                d1: 0,
                d2: 0,
                d3: 0,
              });
              expect(values[1]).to.deep.equal({
                d1: 1,
                d2: 1,
                d3: 1,
              });
              expect(values[2]).to.deep.equal({
                d2: 2,
              });
              expect(values[3]).to.deep.equal(null);
            });

            it("/a/b/c/d2", async () => {
              const values = await performActions(["a", "b", "c", "d2"]);
              expect(values).to.have.length(4);
              expect(values[0]).to.deep.equal(0);
              expect(values[1]).to.deep.equal(1);
              expect(values[2]).to.deep.equal(2);
              expect(values[3]).to.deep.equal(null);
            });

          });

          describe("on /a/b while observing", () => {

            async function performActions(lookup: string[]) {
              const db = await dbBuilder<IndexSchema>({
                a: {
                  b: {
                    c: {
                      d1: 0,
                      d2: 0,
                      d3: 0,
                    },
                  },
                },
              });
              const valuesSubject = new Subject<any | null>();
              db.observe(lookup as any).subscribe(valuesSubject);
              const valuesPromise = valuesSubject.pipe(toArray()).toPromise();
              await db.remove(["__force_db_tick__"]);
              const expectSet = {
                c: {
                  d1: 1,
                  d2: 1,
                  d3: 1,
                },
              };
              await db.set(["a", "b"], expectSet);
              const expectUpdate = {
                c: {
                  d2: 2,
                },
              };
              await db.update(["a", "b"], expectUpdate);
              await db.remove(["a", "b"]);
              const expectPush = {
                d1: 3,
                d2: 3,
                d3: 3,
              };
              const pushKey = await db.push(["a", "b"], expectPush) as string;
              expect(pushKey).to.be.a("string");
              valuesSubject.complete();
              return valuesPromise;
            }

            it("/a", async () => {
              const values = await performActions(["a"]);
              expect(values).to.have.length(5);
              expect(values[0]).to.deep.equal({
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              });
              expect(values[1]).to.deep.equal({
                b: {
                  c: {
                    d1: 1,
                    d2: 1,
                    d3: 1,
                  },
                },
              });
              expect(values[2]).to.deep.equal({
                b: {
                  c: {
                    d2: 2,
                  },
                },
              });
              expect(values[3]).to.deep.equal(null);
              const [key] = Object.keys(values[4].b);
              expect(key).to.be.a("string");
              expect(values[4]).to.deep.equal({
                b: {
                  [key]: {
                    d1: 3,
                    d2: 3,
                    d3: 3,
                  },
                },
              });
            });

            it("/a/b", async () => {
              const values = await performActions(["a", "b"]);
              expect(values).to.have.length(5);
              expect(values[0]).to.deep.equal({
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              });
              expect(values[1]).to.deep.equal({
                c: {
                  d1: 1,
                  d2: 1,
                  d3: 1,
                },
              });
              expect(values[2]).to.deep.equal({
                c: {
                  d2: 2,
                },
              });
              expect(values[3]).to.deep.equal(null);
              const [key] = Object.keys(values[4]);
              expect(key).to.be.a("string");
              expect(values[4]).to.deep.equal({
                [key]: {
                  d1: 3,
                  d2: 3,
                  d3: 3,
                },
              });
            });

            it("/a/b/c", async () => {
              const values = await performActions(["a", "b", "c"]);
              expect(values).to.have.length(4);
              expect(values[0]).to.deep.equal({
                d1: 0,
                d2: 0,
                d3: 0,
              });
              expect(values[1]).to.deep.equal({
                d1: 1,
                d2: 1,
                d3: 1,
              });
              expect(values[2]).to.deep.equal({
                d2: 2,
              });
              expect(values[3]).to.deep.equal(null);
            });

            it("/a/b/c/d2", async () => {
              const values = await performActions(["a", "b", "c", "d2"]);
              expect(values).to.have.length(4);
              expect(values[0]).to.deep.equal(0);
              expect(values[1]).to.deep.equal(1);
              expect(values[2]).to.deep.equal(2);
              expect(values[3]).to.deep.equal(null);
            });

          });

          describe("on /a/b/c while observing", () => {

            async function performActions(lookup: string[]) {
              const db = await dbBuilder<IndexSchema>({
                a: {
                  b: {
                    c: {
                      d1: 0,
                      d2: 0,
                      d3: 0,
                    },
                  },
                },
              });
              const valuesSubject = new Subject<any | null>();
              db.observe(lookup as any).subscribe(valuesSubject);
              const valuesPromise = valuesSubject.pipe(toArray()).toPromise();
              await db.remove(["__force_db_tick__"]);
              const expectSet = {
                d1: 1,
                d2: 1,
                d3: 1,
              };
              await db.set(["a", "b", "c"], expectSet);
              const expectUpdate = {
                d2: 2,
              };
              await db.update(["a", "b", "c"], expectUpdate);
              await db.remove(["a", "b", "c"]);
              const pushKey = await db.push(["a", "b", "c"], 3) as string;
              expect(pushKey).to.be.a("string");
              valuesSubject.complete();
              return valuesPromise;
            }

            it("/a", async () => {
              const values = await performActions(["a"]);
              expect(values).to.have.length(5);
              expect(values[0]).to.deep.equal({
                b: {
                  c: {
                    d1: 0,
                    d2: 0,
                    d3: 0,
                  },
                },
              });
              expect(values[1]).to.deep.equal({
                b: {
                  c: {
                    d1: 1,
                    d2: 1,
                    d3: 1,
                  },
                },
              });
              expect(values[2]).to.deep.equal({
                b: {
                  c: {
                    d1: 1,
                    d2: 2,
                    d3: 1,
                  },
                },
              });
              expect(values[3]).to.deep.equal(null);
              const [key] = Object.keys(values[4].b.c);
              expect(key).to.be.a("string");
              expect(values[4]).to.deep.equal({
                b: {
                  c: {
                    [key]: 3,
                  },
                },
              });
            });

            it("/a/b", async () => {
              const values = await performActions(["a", "b"]);
              expect(values).to.have.length(5);
              expect(values[0]).to.deep.equal({
                c: {
                  d1: 0,
                  d2: 0,
                  d3: 0,
                },
              });
              expect(values[1]).to.deep.equal({
                c: {
                  d1: 1,
                  d2: 1,
                  d3: 1,
                },
              });
              expect(values[2]).to.deep.equal({
                c: {
                  d1: 1,
                  d2: 2,
                  d3: 1,
                },
              });
              expect(values[3]).to.deep.equal(null);
              const [key] = Object.keys(values[4].c);
              expect(key).to.be.a("string");
              expect(values[4]).to.deep.equal({
                c: {
                  [key]: 3,
                },
              });
            });

            it("/a/b/c", async () => {
              const values = await performActions(["a", "b", "c"]);
              expect(values).to.have.length(5);
              expect(values[0]).to.deep.equal({
                d1: 0,
                d2: 0,
                d3: 0,
              });
              expect(values[1]).to.deep.equal({
                d1: 1,
                d2: 1,
                d3: 1,
              });
              expect(values[2]).to.deep.equal({
                d1: 1,
                d2: 2,
                d3: 1,
              });
              expect(values[3]).to.deep.equal(null);
              const [key] = Object.keys(values[4]);
              expect(key).to.be.a("string");
              expect(values[4]).to.deep.equal({
                [key]: 3,
              });
            });

            it("/a/b/c/d2", async () => {
              const values = await performActions(["a", "b", "c", "d2"]);
              expect(values).to.have.length(4);
              expect(values[0]).to.deep.equal(0);
              expect(values[1]).to.deep.equal(1);
              expect(values[2]).to.deep.equal(2);
              expect(values[3]).to.deep.equal(null);
            });

          });

        });

      });

      describe("children", () => {

        describe("(while push, set, update, remove)", () => {

          describe("at /a", () => {

            async function performActions(event: "added" | "removed" | "changed") {
              const db = await dbBuilder<IndexSchema>();
              const valuesSubject = new Subject<Entry<string, IndexSchema["a"]["b"]>>();
              db.children(["a"])[event]().subscribe(valuesSubject);
              const valuesPromise = valuesSubject.pipe(toArray()).toPromise();
              const pushKey = await db.push(["a"], {
                c1: {
                  d1: 11,
                },
              }) as string;
              expect(pushKey).to.be.a("string");
              await db.set(["a"], {
                b2: {
                  c1: {
                    d1: 211,
                  },
                },
                [pushKey]: {
                  c1: {
                    d1: 110,
                  },
                },
              });
              await db.update(["a", "b2"], {
                c1: {
                  d1: 2110,
                },
              });
              await db.set(["a"], {
                b2: {
                  c1: {
                    d1: 21100,
                  },
                },
              });
              await db.remove(["a", "b2"]);
              valuesSubject.complete();
              return valuesPromise;
            }

            it("added", async () => {
              const values = await performActions("added");
              expect(values).to.have.length(2);
              expect(values[0].key).to.be.a("string");
              expect(values[0].value).to.deep.equal({
                c1: {
                  d1: 11,
                },
              });
              expect(values[1]).to.deep.equal({
                key: "b2",
                value: {
                  c1: {
                    d1: 211,
                  },
                },
              });
            });

            it("changed", async () => {
              const values = await performActions("changed");
              expect(values).to.have.length(3);
              expect(values[0].key).to.be.a("string");
              expect(values[0].value).to.deep.equal({
                c1: {
                  d1: 110,
                },
              });
              expect(values[1]).to.deep.equal({
                key: "b2",
                value: {
                  c1: {
                    d1: 2110,
                  },
                },
              });
              expect(values[2]).to.deep.equal({
                key: "b2",
                value: {
                  c1: {
                    d1: 21100,
                  },
                },
              });
            });

            it("removed", async () => {
              const values = await performActions("removed");
              expect(values).to.have.length(2);
              expect(values[0].key).to.be.a("string");
              expect(values[0].value).to.deep.equal({
                c1: {
                  d1: 110,
                },
              });
              expect(values[1]).to.deep.equal({
                key: "b2",
                value: {
                  c1: {
                    d1: 21100,
                  },
                },
              });
            });

          });

          describe("at /a/b", () => {

            async function performActions(event: "added" | "removed" | "changed") {
              const db = await dbBuilder<IndexSchema>();
              const valuesSubject = new Subject<Entry<string, IndexSchema["a"]["b"]["c"]>>();
              db.children(["a", "b"])[event]().subscribe(valuesSubject);
              const valuesPromise = valuesSubject.pipe(toArray()).toPromise();
              const pushKey = await db.push(["a"], {
                c1: {
                  d1: 11,
                },
              }) as string;
              expect(pushKey).to.be.a("string");
              await db.set(["a"], {
                b: {
                  c1: {
                    d1: 211,
                  },
                },
                [pushKey]: {
                  c1: {
                    d1: 110,
                  },
                },
              });
              await db.update(["a", "b"], {
                c1: {
                  d1: 2110,
                },
              });
              await db.set(["a"], {
                b: {
                  c1: {
                    d1: 21100,
                  },
                },
              });
              await db.remove(["a", "b"]);
              valuesSubject.complete();
              return valuesPromise;
            }

            it("added", async () => {
              const values = await performActions("added");
              expect(values).to.have.length(1);
              expect(values[0]).to.deep.equal({
                key: "c1",
                value: {
                  d1: 211,
                },
              });
            });

            it("changed", async () => {
              const values = await performActions("changed");
              expect(values).to.have.length(2);
              expect(values[0]).to.deep.equal({
                key: "c1",
                value: {
                  d1: 2110,
                },
              });
              expect(values[1]).to.deep.equal({
                key: "c1",
                value: {
                  d1: 21100,
                },
              });
            });

            it("removed", async () => {
              const values = await performActions("removed");
              expect(values).to.have.length(1);
              expect(values[0]).to.deep.equal({
                key: "c1",
                value: {
                  d1: 21100,
                },
              });
            });

          });

        });

      });


    });

  });

}
