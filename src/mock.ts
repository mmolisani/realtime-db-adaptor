/// <reference path="../types/targaryen.d.ts"/>
import { concat, from, Observable, ReplaySubject, Subject } from "rxjs";
import { filter, map } from "rxjs/operators";
import targaryen from "targaryen";
import { Changeset, RealtimeDatabase, RealtimeDatabaseChildReference } from "./database";
import { Ruleset } from "./database.spec";
import { asPath } from "./firebase";

const isEmpty = (node: object): node is {} => {
  return Object.keys(node).length === 0;
};

interface Delta<T> {
  old: T;
  new: T;
}

interface WriteEvent<T> {
  path: string;
  delta: Delta<T>;
}

export interface Datastore<SCHEMA extends object> {
  data: Changeset<SCHEMA>;
  events: Subject<WriteEvent<any>>;
}

type DataValue = number | string | boolean | null;
interface ValueMapObject { [key: string]: Value }
type Value = DataValue | ValueMapObject;

const isValueMapObject = (value: Value): value is ValueMapObject => {
  return value !== null && typeof value === "object";
};

const get = (obj: ValueMapObject, key: string): Value => {
  const val = obj[key];
  if (val === void 0) {
    return null;
  }
  return val;
};

type EntryCallback<T = any> = (key: string, value: Value) => T;

const mapOverKeys = <T>(obj: ValueMapObject, callback: EntryCallback<T>) => {
  return Object.keys(obj).map((key) => callback(key, get(obj, key)));
};

type DiffCallback<T = any> = (key: string, a: Value, b: Value) => T;

const forEachOverDiff = (a: ValueMapObject, b: ValueMapObject, aCallback: DiffCallback, bCallback: EntryCallback) => {
  const aKeys = new Set(Object.keys(a));
  for (const key of aKeys) {
    aCallback(key, get(a, key), get(b, key));
  }
  Object.keys(b).forEach((key) => {
    if (!aKeys.has(key)) {
      bCallback(key, get(b, key));
    }
  });
};

const nestValueWithinLookup = (lookup: string[], value: any): any => {
  if (lookup.length === 0) {
    return value;
  }
  const key = lookup[lookup.length - 1];
  const nextLookup = lookup.slice(0, lookup.length - 1);
  return nestValueWithinLookup(nextLookup, { [key]: value });
};

export class Table {
  readonly events: Subject<WriteEvent<any>>;
  private _map = new Map<string, DataValue | Table>();
  constructor(data: ValueMapObject = {}, events: Subject<WriteEvent<any>> = new Subject(), private _name: string = "", private _parent?: Table) {
    this.events = events;
    Object.keys(data).forEach((key) => {
      const subValue = get(data, key);
      if (subValue !== null) {
        const value = this._merge(key, void 0, subValue);
        this._map.set(key, value);
      }
    });
  }
  get path(): string {
    if (this._parent) {
      return `${this._parent.path}/${this._name}`;
    }
    return this._name;
  }
  get(lookup: string[]): Value {
    const [key, ...next] = lookup;
    const sub = this._map.get(key);
    if (sub instanceof Table) {
      if (next.length === 0) {
        return sub.toJSON();
      }
      return sub.get(next);
    }
    if (sub === void 0) {
      return null;
    }
    return sub;
  }
  postEmit(key: string, oldValue: Value, newValue: Value): void {
    this.events.next({
      path: `${this.path}/${key}`,
      delta: {
        old: oldValue,
        new: newValue,
      },
    });
  }
  emit(key: string, oldValue: Value, newValue: Value): void {
    this.events.next({
      path: `${this.path}/${key}`,
      delta: {
        old: oldValue,
        new: newValue,
      },
    });
    // down
    if (isValueMapObject(oldValue)) {
      if (isValueMapObject(newValue)) {
        forEachOverDiff(newValue, oldValue, (subKey, a, b) => {
          this.emit(`${key}/${subKey}`, b, a);
        }, (subKey, val) => {
          this.emit(`${key}/${subKey}`, val, null);
        });
      } else {
        mapOverKeys(oldValue, (subKey, val) => {
          this.emit(`${key}/${subKey}`, val, null);
        });
      }
    } else {
      if (isValueMapObject(newValue)) {
        mapOverKeys(newValue, (subKey, val) => {
          this.emit(`${key}/${subKey}`, null, val);
        });
      } else {
        // no-op
      }
    }
  }
  clear(): void {
    for (const [key, value] of this._map.entries()) {
      let old: Value;
      if (value instanceof Table) {
        old = value.toJSON();
        value.clear();
      } else {
        old = value;
      }
      this.emit(key, old, null);
    }
  }
  private _merge(key: string, oldValue: DataValue | Table | undefined, newValue: NonNullable<Value>): DataValue | Table {
    if (typeof newValue === "object") {
      const table = new Table(newValue, this.events, key, this);
      return table;
    }
    if (oldValue instanceof Table) {
      oldValue.clear();
    }
    return newValue;
  }
  set(key: string, value: Value): this {
    const current = this._map.get(key);
    const old = (current instanceof Table) ? current.toJSON() : (current !== void 0) ? current : null;
    if (value === null) {
      this._map.delete(key);
      this.emit(key, old, null);
      return this;
    }
    const newValue = this._merge(key, current, value);
    this._map.set(key, newValue);
    this.emit(key, old, value);
    return this;
  }
  setAtLookup(lookup: string[], value: Value): this {
    if (lookup.length === 1) {
      return this.set(lookup[0], value);
    }
    const [key, ...next] = lookup;
    const sub = this._map.get(key);
    if (sub instanceof Table) {
      const oldValue = sub.toJSON();
      sub.setAtLookup(next, value);
      let newSub = this._map.get(key);
      if (newSub === void 0) {
        newSub = null;
      }
      const newValue = (newSub instanceof Table) ? newSub.toJSON() : newSub;
      this.postEmit(key, oldValue, newValue);
      return this;
    }
    const newSub = nestValueWithinLookup(next, value);
    this.set(key, newSub);
    return this;
  }
  setFromObject(obj: ValueMapObject): this {
    Object.keys(obj).forEach((key) => {
      this.set(key, obj[key]);
    });
    return this;
  }
  isEmpty(): boolean {
    return this._map.size === 0;
  }
  toJSON(): ValueMapObject | null {
    const obj: ValueMapObject = {};
    for (const [key, val] of this._map.entries()) {
      if (val instanceof Table) {
        if (!val.isEmpty()) {
          obj[key] = val.toJSON();
        }
      } else {
        obj[key] = val;
      }
    }
    if (isEmpty(obj)) {
      return null;
    }
    return obj;
  }
}

const isNullOrUndefined = (value: any): value is null | undefined => {
  return value === null || value === void 0;
};

const isCreate = <T>(delta: Delta<T>): boolean => {
  return isNullOrUndefined(delta.old) && !isNullOrUndefined(delta.new);
};

const isDelete = <T>(delta: Delta<T>): boolean => {
  return !isNullOrUndefined(delta.old) && isNullOrUndefined(delta.new);
};

const isChange = <T>(delta: Delta<T>): boolean => {
  return !(isCreate(delta) || isDelete(delta));
};

class PermissionDeniedError extends Error {
  readonly code = "PERMISSION_DENIED";
}

export class MockDatabase<SCHEMA extends object> implements RealtimeDatabase<SCHEMA> {
  constructor(private _table: Table, private _rules: targaryen.Ruleset, private _auth: targaryen.Auth, private _keygen: () => string) {}
  get auth(): firebase.UserInfo | null {
    return this._auth as firebase.UserInfo | null;
  }
  private _checkAccess() {
    return targaryen.database<SCHEMA>(this._rules, this._table.toJSON() as SCHEMA).as(this._auth);
  }
  async get(lookup: string[]): Promise<any | null> {
    const check = this._checkAccess();
    const read = check.read(asPath(lookup));
    if (!read.allowed) {
      throw new PermissionDeniedError(read.info);
    }
    return this._table.get(lookup);
  }
  async set(lookup: string[], value: any): Promise<void> {
    const check = this._checkAccess();
    const write = check.write(asPath(lookup), value);
    if (!write.allowed) {
      throw new PermissionDeniedError(write.info);
    }
    this._table.setAtLookup(lookup, value);
  }
  async push(lookup: string[], value: any): Promise<string | null> {
    const key = this._keygen();
    const childLookup = [...lookup, key];
    await this.set(childLookup, value);
    return key;
  }
  async update(lookup: string[], value: any): Promise<void> {
    await Promise.all(Object.keys(value).map((key) => {
      return this.set([...lookup, ...key.split("/")], value[key]);
    }));
    return;
  }
  async tx(lookup: string[], update: (currentValue: any) => any | undefined): Promise<boolean> {
    const value = await this.get(lookup);
    const newValue = update(value);
    if (newValue === void 0) {
      return false;
    }
    await this.set(lookup, newValue);
    return true;
  }
  async remove(lookup: string[]): Promise<void> {
    await this.set(lookup, null);
    return;
  }
  observe(lookup: string[]): Observable<any | null> {
    const path = asPath(lookup);
    const subject = new ReplaySubject<Changeset<any> | null>();
    this._table.events.pipe(
      filter((event) => event.path === path),
      map((event) => event.delta.new),
    ).subscribe(subject);
    return concat(from(this.get(lookup)), subject);
  }
  private _childrenEvents(lookup: string[]): Observable<WriteEvent<any>> {
    const pathPattern = new RegExp(`^${asPath(lookup)}/([^\\/\\s]+)$`);
    return this._table.events.pipe(filter((event) => pathPattern.test(event.path)));
  }
  children(lookup: string[]): RealtimeDatabaseChildReference<any, string, any> {
    const events = this._childrenEvents(lookup);
    return {
      lookup,
      added() {
        return events.pipe(
          filter((event) => isCreate(event.delta)),
          map((event) => {
            return {
              key: event.path.substring(event.path.lastIndexOf("/") + 1),
              value: event.delta.new,
            };
          }),
        );
      },
      changed() {
        return events.pipe(
          filter((event) => isChange(event.delta)),
          map((event) => {
            return {
              key: event.path.substring(event.path.lastIndexOf("/") + 1),
              value: event.delta.new,
            };
          }),
        );
      },
      removed() {
        return events.pipe(
          filter((event) => isDelete(event.delta)),
          map((event) => {
            return {
              key: event.path.substring(event.path.lastIndexOf("/") + 1),
              value: event.delta.old,
            };
          }),
        );
      },
    };
  }
}

export const buildDummyKeygen = () => {
  let keyCounter = 0;
  return () => `key-${keyCounter++}`;
};

export const DEFAULT_RULES = {
  ".read": true,
  ".write": true,
};

export function buildMockDatabase<SCHEMA extends object>(data: Changeset<SCHEMA> = {}, rules: Ruleset<SCHEMA> = DEFAULT_RULES as Ruleset<SCHEMA>, auth: targaryen.Auth = null): RealtimeDatabase<SCHEMA> {
  const dummyKeygen = buildDummyKeygen();
  const table = new Table(data as ValueMapObject);
  return new MockDatabase(table, { rules }, auth, dummyKeygen);
}
