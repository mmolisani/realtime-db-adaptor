import { Subscribable } from "./subscriber";

export interface Entry<K extends string, T> {
  key: K;
  value: T;
}

export type Changeset<T> = {
  [K in keyof T]?: Changeset<T[K]> | null;
};

/**
 * Reference used to subscribe to child events beneath a certain lookup.
 *
 * @typeparam L reference to parent lookup
 * @typeparam K key type of all children at this lookup
 * @typeparam T type of all children at this lookup
 */
export interface RealtimeDatabaseChildReference<L extends string[], K extends string, T> {
  /**
   * Readonly tuple of path components
   */
  readonly lookup: Readonly<L>;
  /**
   * Subscribes to all new children added beneath the specified lookup.
   */
  added(): Subscribable<Entry<K, T>>;
  /**
   * Subscribes to all children removed from beneath the specified lookup.
   */
  removed(): Subscribable<Entry<K, T>>;
  /**
   * Subscribes to all value changes for children beneath the specified lookup.
   */
  changed(): Subscribable<Entry<K, T>>;
}

/**
 * Takes in current value of lookup and returns desired new value (or `undefined` for no write).
 * 
 * @typeparam T type of value at lookup
 * @param currentValue Value of data at lookup (or `null` if does not exist)
 */
export interface TransactionHandler<T> {
  (currentValue: T | null): T | undefined;
}

/**
 * Represents the known information about a user. An alias for []`firebase.UserInfo`](https://firebase.google.com/docs/reference/js/firebase.UserInfo).
 */
export interface UserInfo {
  displayName: string | null;
  email: string | null;
  phoneNumber: string | null;
  photoURL: string | null;
  providerId: string;
  /**
   * The user's unique ID.
   */
  uid: string;
}

/**
 * RealtimeDatabase is a JSON data store that allows for active subscriptions to data events.
 * It is primarily modeled after (and a wrapper around) Firebase's [Realtime Database](https://firebase.google.com/products/realtime-database/).
 *
 * This interface expects a type argument that defines the structure of the database.
 * All methods use this schema to provide type information on database access.
 * 
 * @typeparam SCHEMA Object that models the structure of the database
 */
export interface RealtimeDatabase<SCHEMA extends object> {
  /**
   * If authenticated, provides the user info associated with this database handler.
   */
  readonly auth: UserInfo | null;
  /**
   * Retrieves the data at the specified lookup.
   * Will return `null` if node is empty.
   *
   * @typeparam A Key of database at depth 1
   * @param lookup Tuple of path components
   */
  get<A extends keyof SCHEMA>(lookup: [A]): Promise<SCHEMA[A] | null>;
  /**
   * Retrieves the data at the specified lookup.
   * Will return `null` if node is empty.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @param lookup Tuple of path components
   */
  get<A extends keyof SCHEMA, B extends keyof SCHEMA[A]>(lookup: [A, B]): Promise<SCHEMA[A][B] | null>;
  /**
   * Retrieves the data at the specified lookup.
   * Will return `null` if node is empty.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @param lookup Tuple of path components
   */
  get<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B]>(lookup: [A, B, C]): Promise<SCHEMA[A][B][C] | null>;
  /**
   * Retrieves the data at the specified lookup.
   * Will return `null` if node is empty.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @param lookup Tuple of path components
   */
  get<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C]>(lookup: [A, B, C, D]): Promise<SCHEMA[A][B][C][D] | null>;
  /**
   * Retrieves the data at the specified lookup.
   * Will return `null` if node is empty.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @param lookup Tuple of path components
   */
  get<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D]>(lookup: [A, B, C, D, E]): Promise<SCHEMA[A][B][C][D][E] | null>;
  /**
   * Retrieves the data at the specified lookup.
   * Will return `null` if node is empty.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @typeparam F Key of database at depth 6
   * @param lookup Tuple of path components
   */
  get<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D], F extends keyof SCHEMA[A][B][C][D][E]>(lookup: [A, B, C, D, E, F]): Promise<SCHEMA[A][B][C][D][E][F] | null>;
  /**
   * Inserts value as new child of specified lookup and returns the key.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @param lookup Tuple of path components
   * @param value New child value to insert
   */
  push<A extends keyof SCHEMA, B extends keyof SCHEMA[A]>(lookup: [A], value: SCHEMA[A][B]): Promise<string | null>;
  /**
   * Inserts value as new child of specified lookup and returns the key.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @param lookup Tuple of path components
   * @param value New child value to insert
   */
  push<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B]>(lookup: [A, B], value: SCHEMA[A][B][C]): Promise<string | null>;
  /**
   * Inserts value as new child of specified lookup and returns the key.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @param lookup Tuple of path components
   * @param value New child value to insert
   */
  push<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C]>(lookup: [A, B, C], value: SCHEMA[A][B][C][D]): Promise<string | null>;
  /**
   * Inserts value as new child of specified lookup and returns the key.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @param lookup Tuple of path components
   * @param value New child value to insert
   */
  push<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D]>(lookup: [A, B, C, D], value: SCHEMA[A][B][C][D][E]): Promise<string | null>;
  /**
   * Inserts value as new child of specified lookup and returns the key.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @typeparam F Key of database at depth 6
   * @param lookup Tuple of path components
   * @param value New child value to insert
   */
  push<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D], F extends keyof SCHEMA[A][B][C][D][E]>(lookup: [A, B, C, D, E], value: SCHEMA[A][B][C][D][E][F]): Promise<string | null>;
  /**
   * Forces a value into the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @param lookup Tuple of path components
   * @param value New value to set
   */
  set<A extends keyof SCHEMA>(lookup: [A], value: SCHEMA[A]): Promise<void>;
  /**
   * Forces a value into the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @param lookup Tuple of path components
   * @param value New value to set
   */
  set<A extends keyof SCHEMA, B extends keyof SCHEMA[A]>(lookup: [A, B], value: SCHEMA[A][B]): Promise<void>;
  /**
   * Forces a value into the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @param lookup Tuple of path components
   * @param value New value to set
   */
  set<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B]>(lookup: [A, B, C], value: SCHEMA[A][B][C]): Promise<void>;
  /**
   * Forces a value into the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @param lookup Tuple of path components
   * @param value New value to set
   */
  set<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C]>(lookup: [A, B, C, D], value: SCHEMA[A][B][C][D]): Promise<void>;
  /**
   * Forces a value into the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @param lookup Tuple of path components
   * @param value New value to set
   */
  set<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D]>(lookup: [A, B, C, D, E], value: SCHEMA[A][B][C][D][E]): Promise<void>;
  /**
   * Forces a value into the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @typeparam F Key of database at depth 6
   * @param lookup Tuple of path components
   * @param value New value to set
   */
  set<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D], F extends keyof SCHEMA[A][B][C][D][E]>(lookup: [A, B, C, D, E, F], value: SCHEMA[A][B][C][D][E][F]): Promise<void>;
  /**
   * Updates the specified lookup with a new value.
   *
   * @typeparam A Key of database at depth 1
   * @param lookup Tuple of path components
   * @param value Partial value to update
   */
  update<A extends keyof SCHEMA>(lookup: [A], value: Partial<SCHEMA[A]>): Promise<void>;
  /**
   * Updates the specified lookup with a new value.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @param lookup Tuple of path components
   * @param value Partial value to update
   */
  update<A extends keyof SCHEMA, B extends keyof SCHEMA[A]>(lookup: [A, B], value: Partial<SCHEMA[A][B]>): Promise<void>;
  /**
   * Updates the specified lookup with a new value.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @param lookup Tuple of path components
   * @param value Partial value to update
   */
  update<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B]>(lookup: [A, B, C], value: Partial<SCHEMA[A][B][C]>): Promise<void>;
  /**
   * Updates the specified lookup with a new value.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @param lookup Tuple of path components
   * @param value Partial value to update
   */
  update<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C]>(lookup: [A, B, C, D], value: Partial<SCHEMA[A][B][C][D]>): Promise<void>;
  /**
   * Updates the specified lookup with a new value.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @param lookup Tuple of path components
   * @param value Partial value to update
   */
  update<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D]>(lookup: [A, B, C, D, E], value: Partial<SCHEMA[A][B][C][D][E]>): Promise<void>;
  /**
   * Updates the specified lookup with a new value.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @typeparam F Key of database at depth 6
   * @param lookup Tuple of path components
   * @param value Partial value to update
   */
  update<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D], F extends keyof SCHEMA[A][B][C][D][E]>(lookup: [A, B, C, D, E, F], value: Partial<SCHEMA[A][B][C][D][E][F]>): Promise<void>;
  /**
   * Pseudo-atomic set operation that reads value before writing.
   *
   * @typeparam A Key of database at depth 1
   * @param lookup Tuple of path components
   * @param update Callback that takes in current value and returns desired new value (or `undefined` for no write)
   */
  tx<A extends keyof SCHEMA>(lookup: [A], update: TransactionHandler<SCHEMA[A]>): Promise<boolean>;
  /**
   * Pseudo-atomic set operation that reads value before writing.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @param lookup Tuple of path components
   * @param update Callback that takes in current value and returns desired new value (or `undefined` for no write)
   */
  tx<A extends keyof SCHEMA, B extends keyof SCHEMA[A]>(lookup: [A, B], update: TransactionHandler<SCHEMA[A][B]>): Promise<boolean>;
  /**
   * Pseudo-atomic set operation that reads value before writing.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @param lookup Tuple of path components
   * @param update Callback that takes in current value and returns desired new value (or `undefined` for no write)
   */
  tx<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B]>(lookup: [A, B, C], update: TransactionHandler<SCHEMA[A][B][C]>): Promise<boolean>;
  /**
   * Pseudo-atomic set operation that reads value before writing.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @param lookup Tuple of path components
   * @param update Callback that takes in current value and returns desired new value (or `undefined` for no write)
   */
  tx<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C]>(lookup: [A, B, C, D], update: TransactionHandler<SCHEMA[A][B][C][D]>): Promise<boolean>;
  /**
   * Pseudo-atomic set operation that reads value before writing.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @param lookup Tuple of path components
   * @param update Callback that takes in current value and returns desired new value (or `undefined` for no write)
   */
  tx<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D]>(lookup: [A, B, C, D, E], update: TransactionHandler<SCHEMA[A][B][C][D][E]>): Promise<boolean>;
  /**
   * Pseudo-atomic set operation that reads value before writing.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @typeparam F Key of database at depth 6
   * @param lookup Tuple of path components
   * @param update Callback that takes in current value and returns desired new value (or `undefined` for no write)
   */
  tx<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D], F extends keyof SCHEMA[A][B][C][D][E]>(lookup: [A, B, C, D, E, F], update: TransactionHandler<SCHEMA[A][B][C][D][E][F]>): Promise<boolean>;
  /**
   * Removes node at specified lookup from database.
   *
   * @typeparam A Key of database at depth 1
   * @param lookup Tuple of path components
   */
  remove<A extends keyof SCHEMA>(lookup: [A]): Promise<void>;
  /**
   * Removes node at specified lookup from database.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @param lookup Tuple of path components
   */
  remove<A extends keyof SCHEMA, B extends keyof SCHEMA[A]>(lookup: [A, B]): Promise<void>;
  /**
   * Removes node at specified lookup from database.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @param lookup Tuple of path components
   */
  remove<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B]>(lookup: [A, B, C]): Promise<void>;
  /**
   * Removes node at specified lookup from database.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @param lookup Tuple of path components
   */
  remove<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C]>(lookup: [A, B, C, D]): Promise<void>;
  /**
   * Removes node at specified lookup from database.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @param lookup Tuple of path components
   */
  remove<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D]>(lookup: [A, B, C, D, E]): Promise<void>;
  /**
   * Removes node at specified lookup from database.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @typeparam F Key of database at depth 6
   * @param lookup Tuple of path components
   */
  remove<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D], F extends keyof SCHEMA[A][B][C][D][E]>(lookup: [A, B, C, D, E, F]): Promise<void>;
  /**
   * Subscribes to all value changes for the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @param lookup Tuple of path components
   */
  observe<A extends keyof SCHEMA>(lookup: [A]): Subscribable<SCHEMA[A] | null>;
  /**
   * Subscribes to all value changes for the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @param lookup Tuple of path components
   */
  observe<A extends keyof SCHEMA, B extends keyof SCHEMA[A]>(lookup: [A, B]): Subscribable<SCHEMA[A][B] | null>;
  /**
   * Subscribes to all value changes for the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @param lookup Tuple of path components
   */
  observe<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B]>(lookup: [A, B, C]): Subscribable<SCHEMA[A][B][C] | null>;
  /**
   * Subscribes to all value changes for the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @param lookup Tuple of path components
   */
  observe<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C]>(lookup: [A, B, C, D]): Subscribable<SCHEMA[A][B][C][D] | null>;
  /**
   * Subscribes to all value changes for the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @param lookup Tuple of path components
   */
  observe<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D]>(lookup: [A, B, C, D, E]): Subscribable<SCHEMA[A][B][C][D][E] | null>;
  /**
   * Subscribes to all value changes for the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @typeparam F Key of database at depth 6
   * @param lookup Tuple of path components
   */
  observe<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D], F extends keyof SCHEMA[A][B][C][D][E]>(lookup: [A, B, C, D, E, F]): Subscribable<SCHEMA[A][B][C][D][E][F] | null>;
  /**
   * Creates a reference to the children at the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @param lookup Tuple of path components
   */
  children<A extends keyof SCHEMA, B extends keyof SCHEMA[A]>(lookup: [A]): RealtimeDatabaseChildReference<[A, B], B, SCHEMA[A][B]>;
  /**
   * Creates a reference to the children at the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @param lookup Tuple of path components
   */
  children<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B]>(lookup: [A, B]): RealtimeDatabaseChildReference<[A, B, C], C, SCHEMA[A][B][C]>;
  /**
   * Creates a reference to the children at the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @param lookup Tuple of path components
   */
  children<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C]>(lookup: [A, B, C]): RealtimeDatabaseChildReference<[A, B, C, D], D, SCHEMA[A][B][C][D]>;
  /**
   * Creates a reference to the children at the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @param lookup Tuple of path components
   */
  children<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D]>(lookup: [A, B, C, D]): RealtimeDatabaseChildReference<[A, B, C, D, E], E, SCHEMA[A][B][C][D][E]>;
  /**
   * Creates a reference to the children at the specified lookup.
   *
   * @typeparam A Key of database at depth 1
   * @typeparam B Key of database at depth 2
   * @typeparam C Key of database at depth 3
   * @typeparam D Key of database at depth 4
   * @typeparam E Key of database at depth 5
   * @typeparam F Key of database at depth 6
   * @param lookup Tuple of path components
   */
  children<A extends keyof SCHEMA, B extends keyof SCHEMA[A], C extends keyof SCHEMA[A][B], D extends keyof SCHEMA[A][B][C], E extends keyof SCHEMA[A][B][C][D], F extends keyof SCHEMA[A][B][C][D][E]>(lookup: [A, B, C, D, E]): RealtimeDatabaseChildReference<[A, B, C, D, E, F], F, SCHEMA[A][B][C][D][E][F]>;
}
